import static org.junit.Assert.*;

import org.junit.Ignore;
import org.junit.Test;
//import java.io.*;
//import java.lang.*;

public class FizzBuzzTest {

	/**
	 * La chiamata al metodo fizzbuzzSequence con parametro intero 1,
	 * deve restituire la stringa "1".
	 */
	@Test
	public void fizzbuzzSequence1() {
		Funzioni f=new Funzioni();
		assertEquals("1",f.fizzbuzzSequence(1));
		return;
	}
	
	@Test
	public void fizzbuzzSequence2() {
		Funzioni f=new Funzioni();
		//assertEquals("1 2",f.fizzbuzzSequence(2));
		
		String testo=f.fizzbuzzSequence(2);
		
		int i=testo.length();
		
		assertEquals("1 2",testo.substring(i-3));
		return;
	}
	
	@Test
	public void fizzbuzzSequence3() {
		Funzioni f=new Funzioni();
		assertEquals("1 2 fizz",f.fizzbuzzSequence(3));
		return;
	}
	
	@Test
	public void fizzbuzzSequence5() {
		Funzioni f=new Funzioni();
		assertEquals("1 2 fizz 4 buzz",f.fizzbuzzSequence(5));
		return;
	}
	
	@Test
	public void fizzbuzzSequence6() {
		Funzioni f=new Funzioni();
		assertEquals("1 2 fizz 4 buzz fizz",f.fizzbuzzSequence(6));
		return;
	}
	
	@Test
	public void fizzbuzzSequence10() {
		Funzioni f=new Funzioni();
		String testo=f.fizzbuzzSequence(10);
		
		int i=testo.length();
		
		assertEquals("fizz buzz",testo.substring(i-9));
		return;
	}
	
	@Test
	public void fizzbuzzSequence15() {
		Funzioni f=new Funzioni();
		String testo=f.fizzbuzzSequence(15);
		
		int i=testo.length();
		
		assertEquals("fizzbuzz",testo.substring(i-8));
		return;
	}
	
	@Test
	public void fizzbuzzSequence100() {
		Funzioni f=new Funzioni();
		String testo=f.fizzbuzzSequence(100);
		
		int i=testo.length();
		
		assertEquals("woof fizz buzz",testo.substring(i-14));
		return;
	}
	
	@Test
	public void fizzbuzzSequenceNegativo() {
		Funzioni f=new Funzioni();
		
		try{
			int n=-1;
			f.fizzbuzzSequence(n);
		}catch (IllegalArgumentException s){
			int x=0;
			String linea=s.toString();
			x=linea.length();
			assertEquals("non accetto numeri negativi",linea.substring(x-27));		
		}
		
	}
	
	@Test
	public void fizzbuzzSequence7() {
		Funzioni f=new Funzioni();
		
		String testo=f.fizzbuzzSequence(7);
		
		int i=testo.length();
		
		assertEquals("woof",testo.substring(i-4));
		return;
	}
	
	@Ignore
	@Test
	public void fizzbuzzSequence35() {
		Funzioni f=new Funzioni();
		
		String testo=f.fizzbuzzSequence(35);
		
		int i=testo.length();
		
		assertEquals("fizzbuzzwoof",testo.substring(i-12));
		return;
	}
	
}
